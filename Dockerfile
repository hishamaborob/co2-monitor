FROM maven:3.6.0-jdk-8-slim as BUILD
COPY . /tmp/
WORKDIR /tmp/

RUN mvn clean package

FROM openjdk:8-jre-alpine

COPY --from=BUILD /tmp/target/co2-sensor-monitor-0.0.1.jar co2-sensor-monitor-0.0.1.jar

ENTRYPOINT ["java", "-jar", "/co2-sensor-monitor-0.0.1.jar"]
