## Simple CO2 Monitoring Service

##### Important Note:
- Unfortunately I don't have enough time to implement a (production) ready service, I barely found some 1.5 hour
to implement this, which I would describe as a PoC.
- Things like validation, exception handling, logging and dynamic configuration are being ignored here. 
If you feel that something is missing here, it's not because I don't consider it important or don't know how to do it.
- Test coverage is around 80%. No integration test for API controllers.
- I didn't understand what you mean by  (receiving at rate of 1 per minute), is it a rate limiter, time-series, or service scalability? 
Anyway unfortunately I didn't have time to spend implementing this requirement or ask questions around it.


##### Overview

- A simple web service based on Java 8, Spring Boot, Maven, and Docker.
- Three services SensorMeasurementsService, SensorStatusService, SensorMetricsService are responsible for different logic.
- Components are decoupled and can evolve independently.
- Data is stored in repositories with in memory implementation

#### Build and Run


In the parent directory, package, test, and build docker image with:
```
docker build -t co2 .
```

Run:
```
docker run -p 8080:8080 co2
```

Test with valid UUID:

Push data:
```
curl --location --request POST 'http://localhost:8080/api/v1/sensors/2fc5ca22-f1d8-46e8-a87c-f1069f6c53d3/measurements' \
--header 'Content-Type: application/json' \
--data-raw '{
    "co2": 2050,
    "time": "2020-12-25T14:54:54+00:00"
}'
```
Read status:
```
curl --location --request GET 'http://localhost:8080/api/v1/sensors/2fc5ca22-f1d8-46e8-a87c-f1069f6c53d3'
```
Read metrics:
```
curl --location --request GET 'http://localhost:8080/api/v1/sensors/2fc5ca22-f1d8-46e8-a87c-f1069f6c53d3/metrics'
```
Read alerts history:
```
curl --location --request GET 'http://localhost:8080/api/v1/sensors/2fc5ca22-f1d8-46e8-a87c-f1069f6c53d3/alerts'
```