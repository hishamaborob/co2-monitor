package sample.monitor.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import sample.monitor.dto.AlertDto;
import sample.monitor.dto.MeasurementDto;
import sample.monitor.dto.Metric30days;
import sample.monitor.dto.StatusDto;
import sample.monitor.entity.Alert;
import sample.monitor.entity.Measurement;
import sample.monitor.entity.Status;
import sample.monitor.service.measurement.SensorMeasurementsService;
import sample.monitor.service.metric.SensorMetricsService;
import sample.monitor.service.status.SensorStatusService;

import java.time.Period;
import java.util.*;
import java.util.stream.Collectors;

// No test for this api class.
// No validation or error handling implemented.
@RestController
@RequestMapping("/api/v1/sensors")
public class SensorApi {

    @Autowired
    private SensorMeasurementsService sensorMeasurementsService;
    @Autowired
    private SensorStatusService sensorStatusService;
    @Autowired
    private SensorMetricsService sensorMetricsService;

    // You got a typo in your API definition (mesurements -> measurements) in case you get 404
    @PostMapping("/{uuid}/measurements")
    public ResponseEntity<Void> measurements(
            @RequestBody MeasurementDto measurementDto, @PathVariable("uuid") final String uuid) {

        UUID sensorUUID = UUID.fromString(uuid);
        Measurement measurement = MeasurementDto.from(measurementDto);
        sensorMeasurementsService.addMeasurement(sensorUUID, measurement);
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @GetMapping("/{uuid}")
    public ResponseEntity<StatusDto> status(@PathVariable("uuid") final String uuid) {

        UUID sensorUUID = UUID.fromString(uuid);
        Status status = sensorStatusService.getSensorStatus(sensorUUID);
        return new ResponseEntity<>(new StatusDto(status), HttpStatus.OK);
    }

    @GetMapping("/{uuid}/metrics")
    public ResponseEntity<Metric30days> metrics30days(@PathVariable("uuid") final String uuid) {

        UUID sensorUUID = UUID.fromString(uuid);
        Period period = Period.ofDays(30);
        Optional<Measurement> measurement = sensorMetricsService.getSensorMaxMeasurement(sensorUUID, period);
        OptionalDouble average = sensorMetricsService.getSensorMeasurementAverage(sensorUUID, period);
        Metric30days metric30days = new Metric30days();
        measurement.ifPresent(m -> metric30days.setMaxLast30Days(m.getCo2()));
        average.ifPresent(avg -> metric30days.setAvgLast30Days(avg));
        return new ResponseEntity<>(metric30days, HttpStatus.OK);
    }

    @GetMapping("/{uuid}/alerts")
    public ResponseEntity<List<AlertDto>> alerts(@PathVariable("uuid") final String uuid) {

        UUID sensorUUID = UUID.fromString(uuid);
        List<Alert> alertList = sensorStatusService.getSensorAlerts(sensorUUID);
        List<AlertDto> alertDtoList = alertList.stream().map(alert -> AlertDto.of(alert)).collect(Collectors.toList());
        return new ResponseEntity<>(alertDtoList, HttpStatus.OK);
    }
}
