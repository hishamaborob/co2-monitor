package sample.monitor.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Configuration
public class ServiceConfig {

    @Bean("sensorStatusServiceExecutor")
    public ExecutorService sensorStatusServiceExecutor() {

        // Suitable for short lived tasks.
        return Executors.newCachedThreadPool();
    }

    @Bean("sensorMeasurementsServiceExecutor")
    public ExecutorService sensorMeasurementsServiceExecutor() {

        // Suitable for short lived tasks.
        return Executors.newCachedThreadPool();
    }
}
