package sample.monitor.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import sample.monitor.entity.Alert;
import sample.monitor.entity.Measurement;

import java.time.LocalDateTime;
import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class AlertDto {

    private LocalDateTime startTime;
    private LocalDateTime endTime;
    // I don't agree with this kind of naming. Rather put in a list, so here I'm sticking to your API definition.
    private Integer measurement1;
    private Integer measurement2;
    private Integer measurement3;

    public AlertDto() {
    }

    public static AlertDto of(Alert alert) {

        AlertDto alertDto = new AlertDto();
        alertDto.startTime = alert.getStartTime();
        alertDto.endTime = alert.getEndTime();
        List<Measurement> measurements = alert.getMeasurements();
        // I was forced to this way of coding here:
        if (measurements.size() == 3) {
            alertDto.measurement3 = measurements.get(2).getCo2();
        }
        if (measurements.size() >= 2) {
            alertDto.measurement2 = measurements.get(1).getCo2();
        }
        if (measurements.size() >= 1) {
            alertDto.measurement1 = measurements.get(0).getCo2();
        }
        return alertDto;
    }

    public LocalDateTime getStartTime() {
        return startTime;
    }

    public void setStartTime(LocalDateTime startTime) {
        this.startTime = startTime;
    }

    public LocalDateTime getEndTime() {
        return endTime;
    }

    public void setEndTime(LocalDateTime endTime) {
        this.endTime = endTime;
    }

    public Integer getMeasurement1() {
        return measurement1;
    }

    public void setMeasurement1(Integer measurement1) {
        this.measurement1 = measurement1;
    }

    public Integer getMeasurement2() {
        return measurement2;
    }

    public void setMeasurement2(Integer measurement2) {
        this.measurement2 = measurement2;
    }

    public Integer getMeasurement3() {
        return measurement3;
    }

    public void setMeasurement3(Integer measurement3) {
        this.measurement3 = measurement3;
    }
}
