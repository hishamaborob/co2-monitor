package sample.monitor.dto;

import sample.monitor.entity.Measurement;

import java.time.LocalDateTime;
import java.time.ZonedDateTime;

public class MeasurementDto {

    private int co2;

    private String time;

    public MeasurementDto() {
    }

    public MeasurementDto(int co2, String time) {
        this.co2 = co2;
        this.time = time;
    }

    public static Measurement from(MeasurementDto measurementDto) {

        ZonedDateTime zdt = ZonedDateTime.parse(measurementDto.time);
        LocalDateTime dateTime = zdt.toLocalDateTime();
        return new Measurement(measurementDto.getCo2(), dateTime);
    }

    public int getCo2() {
        return co2;
    }

    public void setCo2(int co2) {
        this.co2 = co2;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }
}
