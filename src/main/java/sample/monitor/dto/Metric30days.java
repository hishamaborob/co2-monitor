package sample.monitor.dto;

import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class Metric30days {

    private Integer maxLast30Days;
    private Double avgLast30Days;

    public Metric30days() {
    }

    public Metric30days(int maxLast30Days, double avgLast30Days) {
        this.maxLast30Days = maxLast30Days;
        this.avgLast30Days = avgLast30Days;
    }

    public Integer getMaxLast30Days() {
        return maxLast30Days;
    }

    public void setMaxLast30Days(Integer maxLast30Days) {
        this.maxLast30Days = maxLast30Days;
    }

    public Double getAvgLast30Days() {
        return avgLast30Days;
    }

    public void setAvgLast30Days(Double avgLast30Days) {
        this.avgLast30Days = avgLast30Days;
    }
}
