package sample.monitor.entity;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

public class Alert {

    private final UUID alertId;
    private final LocalDateTime startTime;
    private final LocalDateTime endTime;
    private final List<Measurement> measurements;

    public Alert(UUID alertId, LocalDateTime startTime, LocalDateTime endTime, List<Measurement> measurements) {
        this.alertId = alertId;
        this.startTime = startTime;
        this.endTime = endTime;
        this.measurements = measurements;
    }

    public UUID getAlertId() {
        return alertId;
    }

    public LocalDateTime getStartTime() {
        return startTime;
    }

    public LocalDateTime getEndTime() {
        return endTime;
    }

    public List<Measurement> getMeasurements() {
        return new ArrayList<>(measurements);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Alert alert = (Alert) o;
        return Objects.equals(alertId, alert.alertId) &&
                Objects.equals(startTime, alert.startTime) &&
                Objects.equals(endTime, alert.endTime) &&
                Objects.equals(measurements, alert.measurements);
    }

    @Override
    public int hashCode() {
        return Objects.hash(alertId, startTime, endTime, measurements);
    }

    @Override
    public String toString() {
        return "Alert{" +
                "alertId=" + alertId +
                ", startTime=" + startTime +
                ", endTime=" + endTime +
                ", measurements=" + measurements +
                '}';
    }
}
