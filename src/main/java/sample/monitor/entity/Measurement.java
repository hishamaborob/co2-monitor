package sample.monitor.entity;


import java.time.LocalDateTime;
import java.util.Objects;

public class Measurement {

    private final int co2;
    private final LocalDateTime time;

    public Measurement(int co2, LocalDateTime time) {
        this.co2 = co2;
        this.time = time;
    }

    public int getCo2() {
        return co2;
    }

    public LocalDateTime getTime() {
        return time;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Measurement that = (Measurement) o;
        return co2 == that.co2 &&
                Objects.equals(time, that.time);
    }

    @Override
    public int hashCode() {
        return Objects.hash(co2, time);
    }

    @Override
    public String toString() {
        return "Measurement{" +
                "co2=" + co2 +
                ", time=" + time +
                '}';
    }
}
