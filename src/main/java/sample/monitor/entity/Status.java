package sample.monitor.entity;

public enum Status {

    OK,
    WARN,
    ALERT,
    UNKNOWN
}
