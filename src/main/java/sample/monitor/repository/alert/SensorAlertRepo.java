package sample.monitor.repository.alert;

import sample.monitor.entity.Alert;

import java.util.Map;
import java.util.Optional;
import java.util.UUID;

public interface SensorAlertRepo {

    void addAlert(UUID sensorUUID, Alert alert);

    void updateAlert(UUID sensorUUID, Alert alert);

    Optional<Alert> getLatestAlert(UUID sensorUUID);

    Map<UUID, Alert> getAlerts(UUID sensorUUID);
}
