package sample.monitor.repository.alert;

import org.springframework.stereotype.Repository;
import sample.monitor.entity.Alert;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

@Repository
public class SensorAlertRepoInMemory implements SensorAlertRepo {

    private Map<UUID, Map<UUID, Alert>> sensorsAlerts = new ConcurrentHashMap<>();

    @Override
    public void addAlert(UUID sensorUUID, Alert alert) {

        sensorsAlerts.putIfAbsent(sensorUUID, alertsMap());
        sensorsAlerts.get(sensorUUID).putIfAbsent(alert.getAlertId(), alert);
    }

    @Override
    public void updateAlert(UUID sensorUUID, Alert alert) {

        sensorsAlerts.get(sensorUUID).put(alert.getAlertId(), alert);
    }

    @Override
    public Optional<Alert> getLatestAlert(UUID sensorUUID) {

        return sensorsAlerts.get(sensorUUID).values().stream()
                .sorted(Comparator.comparing(Alert::getStartTime).reversed()).findFirst();
    }

    @Override
    public Map<UUID, Alert> getAlerts(UUID sensorUUID) {

        return new HashMap<>(sensorsAlerts.getOrDefault(sensorUUID, new HashMap<>()));
    }

    private Map<UUID, Alert> alertsMap() {

        return Collections.synchronizedMap(new HashMap<>());
    }
}
