package sample.monitor.repository.measurement;

import sample.monitor.entity.Measurement;

import java.util.List;
import java.util.Optional;
import java.util.SortedSet;
import java.util.UUID;

public interface SensorMeasurementsRepo {

    void addMeasurement(UUID sensorUUID, Measurement measurement);

    Optional<Measurement> getLatestMeasurement(UUID sensorUUID);

    List<Measurement> getLatestMeasurements(UUID sensorUUID, int limit);

    SortedSet<Measurement> getMeasurements(UUID sensorUUID);

}
