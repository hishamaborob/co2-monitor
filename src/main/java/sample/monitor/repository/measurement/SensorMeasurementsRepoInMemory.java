package sample.monitor.repository.measurement;

import org.springframework.stereotype.Repository;
import sample.monitor.entity.Measurement;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

@Repository
public class SensorMeasurementsRepoInMemory implements SensorMeasurementsRepo {

    private Map<UUID, SortedSet<Measurement>> sensorsMeasurements = new ConcurrentHashMap<>();

    @Override
    public void addMeasurement(UUID sensorUUID, Measurement measurement) {

        sensorsMeasurements.putIfAbsent(sensorUUID, measurementsSet());
        sensorsMeasurements.get(sensorUUID).add(measurement);
    }

    @Override
    public Optional<Measurement> getLatestMeasurement(UUID sensorUUID) {

        Optional<Measurement> measurementOptional = Optional.empty();
        if (sensorsMeasurements.containsKey(sensorUUID)) {
            measurementOptional = Optional.ofNullable(sensorsMeasurements.get(sensorUUID).first());
        }
        return measurementOptional;
    }

    @Override
    public List<Measurement> getLatestMeasurements(UUID sensorUUID, int limit) {

        List<Measurement> measurementList = new ArrayList<>();
        if (sensorsMeasurements.containsKey(sensorUUID)) {
            measurementList = sensorsMeasurements.get(sensorUUID).stream().limit(limit).collect(Collectors.toList());
        }
        return measurementList;
    }

    @Override
    public SortedSet<Measurement> getMeasurements(UUID sensorUUID) {

        return new TreeSet<>(sensorsMeasurements.getOrDefault(sensorUUID, new TreeSet<>()));
    }

    private SortedSet<Measurement> measurementsSet() {

        return Collections.synchronizedSortedSet(new TreeSet<>(Comparator.comparing(Measurement::getTime).reversed()));
    }
}
