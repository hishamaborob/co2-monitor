package sample.monitor.repository.status;

import sample.monitor.entity.Status;

import java.util.UUID;

public interface SensorStatusRepo {

    void setSensorStatus(UUID sensorUUID, Status status);

    Status getSensorStatus(UUID sensorUUID);
}
