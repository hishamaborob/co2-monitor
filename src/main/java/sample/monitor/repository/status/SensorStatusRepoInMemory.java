package sample.monitor.repository.status;

import org.springframework.stereotype.Repository;
import sample.monitor.entity.Status;

import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

import static sample.monitor.entity.Status.UNKNOWN;

@Repository
public class SensorStatusRepoInMemory implements SensorStatusRepo {

    private Map<UUID, Status> sensorStatus = new ConcurrentHashMap<>();

    @Override
    public void setSensorStatus(UUID sensorUUID, Status status) {

        sensorStatus.put(sensorUUID, status);
    }

    @Override
    public Status getSensorStatus(UUID sensorUUID) {

        return sensorStatus.getOrDefault(sensorUUID, UNKNOWN);
    }
}
