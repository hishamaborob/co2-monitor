package sample.monitor.service.measurement;

import sample.monitor.entity.Measurement;

import java.util.List;
import java.util.UUID;

public interface SensorMeasurementsService {

    void addMeasurement(UUID sensorUUID, Measurement measurement);

    List<Measurement> getMeasurements(UUID sensorUUID, int limit);
}
