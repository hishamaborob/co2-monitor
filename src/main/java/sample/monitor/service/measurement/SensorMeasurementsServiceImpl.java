package sample.monitor.service.measurement;

import sample.monitor.entity.Measurement;
import sample.monitor.repository.measurement.SensorMeasurementsRepo;
import sample.monitor.service.status.SensorStatusService;

import java.util.List;
import java.util.UUID;

public class SensorMeasurementsServiceImpl implements SensorMeasurementsService {

    protected SensorMeasurementsRepo sensorMeasurementsRepo;
    protected SensorStatusService sensorStatusService;

    public SensorMeasurementsServiceImpl(
            SensorMeasurementsRepo sensorMeasurementsRepo,
            SensorStatusService sensorStatusService) {
        this.sensorMeasurementsRepo = sensorMeasurementsRepo;
        this.sensorStatusService = sensorStatusService;
    }

    @Override
    public void addMeasurement(UUID sensorUUID, Measurement measurement) {

        sensorMeasurementsRepo.addMeasurement(sensorUUID, measurement);
        sensorStatusService.updateSensorStatus(sensorUUID);
    }

    @Override
    public List<Measurement> getMeasurements(UUID sensorUUID, int limit) {

        return sensorMeasurementsRepo.getLatestMeasurements(sensorUUID, limit);
    }
}
