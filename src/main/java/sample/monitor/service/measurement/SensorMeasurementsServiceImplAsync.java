package sample.monitor.service.measurement;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import sample.monitor.entity.Measurement;
import sample.monitor.repository.measurement.SensorMeasurementsRepo;
import sample.monitor.service.status.SensorStatusService;

import java.util.UUID;
import java.util.concurrent.ExecutorService;

@Service
public class SensorMeasurementsServiceImplAsync
        extends SensorMeasurementsServiceImpl implements SensorMeasurementsService {

    private ExecutorService executorService;

    @Autowired
    public SensorMeasurementsServiceImplAsync(
            SensorMeasurementsRepo sensorMeasurementsRepo,
            SensorStatusService sensorStatusService,
            @Qualifier("sensorMeasurementsServiceExecutor") ExecutorService executorService) {
        super(sensorMeasurementsRepo, sensorStatusService);
        this.executorService = executorService;
    }

    @Override
    public void addMeasurement(UUID sensorUUID, Measurement measurement) {

        executorService.submit(() -> super.addMeasurement(sensorUUID, measurement));
    }
}
