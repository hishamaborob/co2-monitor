package sample.monitor.service.metric;

import sample.monitor.entity.Measurement;

import java.time.Period;
import java.util.Optional;
import java.util.OptionalDouble;
import java.util.UUID;

public interface SensorMetricsService {

    Optional<Measurement> getSensorMaxMeasurement(UUID sensorUUID, Period period);

    OptionalDouble getSensorMeasurementAverage(UUID sensorUUID, Period period);
}
