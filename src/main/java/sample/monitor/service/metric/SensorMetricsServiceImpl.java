package sample.monitor.service.metric;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import sample.monitor.entity.Measurement;
import sample.monitor.repository.measurement.SensorMeasurementsRepo;

import java.time.LocalDateTime;
import java.time.Period;
import java.util.Optional;
import java.util.OptionalDouble;
import java.util.SortedSet;
import java.util.UUID;

import static java.util.Comparator.comparing;

@Service
public class SensorMetricsServiceImpl implements SensorMetricsService {

    private SensorMeasurementsRepo sensorMeasurementsRepo;

    @Autowired
    public SensorMetricsServiceImpl(SensorMeasurementsRepo sensorMeasurementsRepo) {
        this.sensorMeasurementsRepo = sensorMeasurementsRepo;
    }

    @Override
    public Optional<Measurement> getSensorMaxMeasurement(UUID sensorUUID, Period period) {

        SortedSet<Measurement> measurementSortedSet = sensorMeasurementsRepo.getMeasurements(sensorUUID);
        return measurementSortedSet.headSet(dummyComparisonMeasurement(period)).stream()
                .max(comparing(Measurement::getCo2));
    }

    @Override
    public OptionalDouble getSensorMeasurementAverage(UUID sensorUUID, Period period) {

        SortedSet<Measurement> measurementSortedSet = sensorMeasurementsRepo.getMeasurements(sensorUUID);
        return measurementSortedSet.headSet(dummyComparisonMeasurement(period)).stream()
                .mapToInt(Measurement::getCo2).average();
    }

    private Measurement dummyComparisonMeasurement(Period period) {
        return new Measurement(1, LocalDateTime.now().minus(period));
    }
}
