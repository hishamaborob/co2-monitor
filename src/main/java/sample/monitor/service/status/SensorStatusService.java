package sample.monitor.service.status;

import sample.monitor.entity.Alert;
import sample.monitor.entity.Status;

import java.util.List;
import java.util.UUID;

public interface SensorStatusService {

    void updateSensorStatus(UUID sensorUUID);

    Status getSensorStatus(UUID sensorUUID);

    List<Alert> getSensorAlerts(UUID sensorUUID);
}
