package sample.monitor.service.status;

import sample.monitor.entity.Alert;
import sample.monitor.entity.Measurement;
import sample.monitor.entity.Status;
import sample.monitor.repository.alert.SensorAlertRepo;
import sample.monitor.repository.measurement.SensorMeasurementsRepo;
import sample.monitor.repository.status.SensorStatusRepo;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

public class SensorStatusServiceImpl implements SensorStatusService {

    private static final int ALERT_THRESHOLD = 3;
    private static final int CO2_THRESHOLD = 2000;
    private SensorMeasurementsRepo sensorMeasurementsRepo;
    private SensorStatusRepo sensorStatusRepo;
    private SensorAlertRepo sensorAlertRepo;

    public SensorStatusServiceImpl(
            SensorMeasurementsRepo sensorMeasurementsRepo,
            SensorStatusRepo sensorStatusRepo, SensorAlertRepo sensorAlertRepo) {
        this.sensorMeasurementsRepo = sensorMeasurementsRepo;
        this.sensorStatusRepo = sensorStatusRepo;
        this.sensorAlertRepo = sensorAlertRepo;
    }

    @Override
    public void updateSensorStatus(UUID sensorUUID) {

        Status status = sensorStatusRepo.getSensorStatus(sensorUUID);
        if (status == Status.OK || status == Status.UNKNOWN || status == Status.WARN) {
            status = updateStatus(sensorUUID);
        }
        if (status == Status.WARN) {
            handleWarnStatus(sensorUUID);
        } else if (status == Status.ALERT) {
            handleAlertStatus(sensorUUID);
        }
    }

    @Override
    public Status getSensorStatus(UUID sensorUUID) {

        return sensorStatusRepo.getSensorStatus(sensorUUID);
    }


    @Override
    public List<Alert> getSensorAlerts(UUID sensorUUID) {

        return sensorAlertRepo.getAlerts(sensorUUID).values().stream().collect(Collectors.toList());
    }

    private Status updateStatus(UUID sensorUUID) {

        Optional<Measurement> measurementOptional = sensorMeasurementsRepo.getLatestMeasurement(sensorUUID);
        if (!measurementOptional.isPresent()) {
            return Status.UNKNOWN;
        }
        Measurement measurement = measurementOptional.get();
        if (measurement.getCo2() > CO2_THRESHOLD) {
            sensorStatusRepo.setSensorStatus(sensorUUID, Status.WARN);
            return Status.WARN;
        } else {
            sensorStatusRepo.setSensorStatus(sensorUUID, Status.OK);
            return Status.OK;
        }
    }

    private void handleWarnStatus(UUID sensorUUID) {

        List<Measurement> measurementList =
                sensorMeasurementsRepo.getLatestMeasurements(sensorUUID, ALERT_THRESHOLD);
        final long count = measurementList.stream()
                .filter(measurement -> measurement.getCo2() > CO2_THRESHOLD).count();
        if (count == ALERT_THRESHOLD) {
            sensorStatusRepo.setSensorStatus(sensorUUID, Status.ALERT);
            sensorAlertRepo.addAlert(
                    sensorUUID, new Alert(UUID.randomUUID(), LocalDateTime.now(), null, measurementList));
        }
    }

    private void handleAlertStatus(UUID sensorUUID) {

        Optional<Alert> alertOptional = sensorAlertRepo.getLatestAlert(sensorUUID);
        if (alertOptional.isPresent()) {
            Alert alert = alertOptional.get();
            List<Measurement> measurementList = sensorMeasurementsRepo.getLatestMeasurements(sensorUUID, ALERT_THRESHOLD);
            final long count = measurementList.stream()
                    .filter(measurement -> measurement.getCo2() <= CO2_THRESHOLD).count();
            if (count == ALERT_THRESHOLD) {
                sensorStatusRepo.setSensorStatus(sensorUUID, Status.OK);
                Alert alertUpdated = new Alert(
                        alert.getAlertId(), alert.getStartTime(), LocalDateTime.now(), alert.getMeasurements());
                sensorAlertRepo.updateAlert(sensorUUID, alertUpdated);
            }
        }
    }
}
