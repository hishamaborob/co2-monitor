package sample.monitor.service.status;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import sample.monitor.repository.alert.SensorAlertRepo;
import sample.monitor.repository.measurement.SensorMeasurementsRepo;
import sample.monitor.repository.status.SensorStatusRepo;

import java.util.UUID;
import java.util.concurrent.ExecutorService;

@Service
public class SensorStatusServiceImplAsync
        extends SensorStatusServiceImpl implements SensorStatusService {

    private ExecutorService executorService;

    @Autowired
    public SensorStatusServiceImplAsync(
            SensorMeasurementsRepo sensorMeasurementsRepo,
            SensorStatusRepo sensorStatusRepo,
            SensorAlertRepo sensorAlertRepo,
            @Qualifier("sensorStatusServiceExecutor") ExecutorService executorService) {
        super(sensorMeasurementsRepo, sensorStatusRepo, sensorAlertRepo);
        this.executorService = executorService;
    }

    @Override
    public void updateSensorStatus(UUID sensorUUID) {

        executorService.submit(() -> super.updateSensorStatus(sensorUUID));
    }
}
