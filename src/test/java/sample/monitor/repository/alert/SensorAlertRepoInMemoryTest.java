package sample.monitor.repository.alert;

import org.junit.Before;
import org.junit.Test;
import sample.monitor.entity.Alert;
import sample.monitor.entity.Measurement;
import sample.monitor.repository.alert.SensorAlertRepo;
import sample.monitor.repository.alert.SensorAlertRepoInMemory;

import java.time.LocalDateTime;
import java.util.*;

import static org.junit.Assert.*;

public class SensorAlertRepoInMemoryTest {

    private SensorAlertRepo sensorAlertRepo;

    @Before
    public void setUp() throws Exception {
        sensorAlertRepo = new SensorAlertRepoInMemory();
    }

    @Test
    public void addAlert() {

        UUID uuid = UUID.randomUUID();
        Alert alert = generateAlert();
        sensorAlertRepo.addAlert(uuid, alert);
        Map<UUID, Alert> alertMap = sensorAlertRepo.getAlerts(uuid);
        assertFalse(alertMap.isEmpty());
        assertEquals(1, alertMap.size());
        assertTrue(alertMap.containsKey(alert.getAlertId()));
        assertEquals(alert, alertMap.get(alert.getAlertId()));
    }

    @Test
    public void updateAlert() {

        UUID uuid = UUID.randomUUID();
        Alert alert = generateAlert();
        Alert alertUpdated = new Alert(alert.getAlertId(), alert.getStartTime(), LocalDateTime.now(), alert.getMeasurements());
        sensorAlertRepo.addAlert(uuid, alert);
        sensorAlertRepo.updateAlert(uuid, alertUpdated);
        Map<UUID, Alert> alertMap = sensorAlertRepo.getAlerts(uuid);
        assertNotNull(alertMap);
        assertFalse(alertMap.isEmpty());
        assertEquals(1, alertMap.size());
        assertTrue(alertMap.containsKey(alertUpdated.getAlertId()));
        assertEquals(alertUpdated, alertMap.get(alertUpdated.getAlertId()));
        assertNotEquals(alert, alertMap.get(alertUpdated.getAlertId()));
    }

    @Test
    public void getLatestAlert() {

        UUID uuid = UUID.randomUUID();
        Alert alert1 = generateAlert();
        Alert alert2 = new Alert(UUID.randomUUID(),
                LocalDateTime.now().plusMinutes(2), LocalDateTime.now().plusMinutes(7), null);
        sensorAlertRepo.addAlert(uuid, alert1);
        sensorAlertRepo.addAlert(uuid, alert2);
        Optional<Alert> alertLatest = sensorAlertRepo.getLatestAlert(uuid);
        assertTrue(alertLatest.isPresent());
        assertEquals(alert2, alertLatest.get());
    }

    @Test
    public void getAlerts() {

        UUID uuid = UUID.randomUUID();
        Alert alert1 = generateAlert();
        Alert alert2 = new Alert(UUID.randomUUID(),
                LocalDateTime.now().plusMinutes(2), LocalDateTime.now().plusMinutes(7), null);
        Map<UUID, Alert> alertMapExpected = new HashMap<>();
        alertMapExpected.put(alert1.getAlertId(), alert1);
        alertMapExpected.put(alert2.getAlertId(), alert2);
        sensorAlertRepo.addAlert(uuid, alert1);
        sensorAlertRepo.addAlert(uuid, alert2);
        Map<UUID, Alert> uuidAlertMap = sensorAlertRepo.getAlerts(uuid);
        assertNotNull(uuidAlertMap);
        assertFalse(uuidAlertMap.isEmpty());
        assertEquals(alertMapExpected, uuidAlertMap);
    }

    private Alert generateAlert() {

        return new Alert(UUID.randomUUID(),
                LocalDateTime.now(), null, Arrays.asList(new Measurement(2000, LocalDateTime.now())));
    }
}