package sample.monitor.repository.measurement;

import org.junit.Before;
import org.junit.Test;
import sample.monitor.entity.Measurement;
import sample.monitor.repository.measurement.SensorMeasurementsRepo;
import sample.monitor.repository.measurement.SensorMeasurementsRepoInMemory;

import java.time.LocalDateTime;
import java.util.*;

import static org.junit.Assert.*;

public class SensorMeasurementsRepoInMemoryTest {

    private SensorMeasurementsRepo sensorMeasurementsRepo;

    @Before
    public void setUp() throws Exception {
        sensorMeasurementsRepo = new SensorMeasurementsRepoInMemory();
    }

    @Test
    public void addMeasurement() {

        UUID uuid = UUID.randomUUID();
        Measurement measurement = new Measurement(2000, LocalDateTime.now());
        sensorMeasurementsRepo.addMeasurement(uuid, measurement);
        SortedSet<Measurement> measurementSet = sensorMeasurementsRepo.getMeasurements(uuid);
        assertNotNull(measurementSet);
        assertFalse(measurementSet.isEmpty());
        assertEquals(1, measurementSet.size());
        assertEquals(measurement, measurementSet.first());
    }

    @Test
    public void getLatestMeasurement() {

        UUID uuid = UUID.randomUUID();
        Measurement measurement1 = new Measurement(2000, LocalDateTime.now());
        Measurement measurement2 = new Measurement(2000, LocalDateTime.now().plusMinutes(2));
        sensorMeasurementsRepo.addMeasurement(uuid, measurement1);
        sensorMeasurementsRepo.addMeasurement(uuid, measurement2);
        Optional<Measurement> measurement = sensorMeasurementsRepo.getLatestMeasurement(uuid);
        assertTrue(measurement.isPresent());
        assertEquals(measurement2, measurement.get());
    }

    @Test
    public void getLatestMeasurements() {

        UUID uuid = UUID.randomUUID();
        Measurement measurement1 = new Measurement(2000, LocalDateTime.now());
        Measurement measurement2 = new Measurement(2000, LocalDateTime.now().plusMinutes(2));
        Measurement measurement3 = new Measurement(2000, LocalDateTime.now().plusMinutes(3));
        List<Measurement> measurementListExpected = Arrays.asList(measurement3, measurement2);
        sensorMeasurementsRepo.addMeasurement(uuid, measurement1);
        sensorMeasurementsRepo.addMeasurement(uuid, measurement2);
        sensorMeasurementsRepo.addMeasurement(uuid, measurement3);
        List<Measurement> measurementList = sensorMeasurementsRepo.getLatestMeasurements(uuid, 2);
        assertNotNull(measurementList);
        assertFalse(measurementList.isEmpty());
        assertEquals(2, measurementList.size());
        assertEquals(measurementListExpected, measurementList);
    }

    @Test
    public void getMeasurements() {

        UUID uuid = UUID.randomUUID();
        Measurement measurement1 = new Measurement(2000, LocalDateTime.now());
        Measurement measurement2 = new Measurement(2000, LocalDateTime.now().plusMinutes(3));
        SortedSet<Measurement> measurementSortedSetExpected =
                new TreeSet<>(Comparator.comparing(Measurement::getTime).reversed());
        measurementSortedSetExpected.addAll(Arrays.asList(measurement1, measurement2));
        sensorMeasurementsRepo.addMeasurement(uuid, measurement1);
        sensorMeasurementsRepo.addMeasurement(uuid, measurement2);
        SortedSet<Measurement> measurementSortedSet = sensorMeasurementsRepo.getMeasurements(uuid);
        assertNotNull(measurementSortedSet);
        assertFalse(measurementSortedSet.isEmpty());
        assertEquals(2, measurementSortedSet.size());
        assertEquals(measurementSortedSetExpected, measurementSortedSet);
    }
}