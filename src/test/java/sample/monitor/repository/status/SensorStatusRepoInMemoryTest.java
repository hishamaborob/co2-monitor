package sample.monitor.repository.status;

import org.junit.Before;
import org.junit.Test;
import sample.monitor.entity.Status;
import sample.monitor.repository.status.SensorStatusRepo;
import sample.monitor.repository.status.SensorStatusRepoInMemory;

import java.util.UUID;

import static org.junit.Assert.*;

public class SensorStatusRepoInMemoryTest {

    private SensorStatusRepo sensorStatusRepo;

    @Before
    public void setUp() throws Exception {
        sensorStatusRepo = new SensorStatusRepoInMemory();
    }

    @Test
    public void setSensorStatus() {

        UUID uuid = UUID.randomUUID();
        Status status = sensorStatusRepo.getSensorStatus(uuid);
        assertEquals(Status.UNKNOWN, status);
        sensorStatusRepo.setSensorStatus(uuid, Status.OK);
        status = sensorStatusRepo.getSensorStatus(uuid);
        assertNotNull(status);
        assertEquals(Status.OK, status);
    }

    @Test
    public void getSensorStatus() {

        UUID uuid = UUID.randomUUID();
        sensorStatusRepo.setSensorStatus(uuid, Status.OK);
        Status status = sensorStatusRepo.getSensorStatus(uuid);
        assertNotNull(status);
        assertEquals(Status.OK, status);
        sensorStatusRepo.setSensorStatus(uuid, Status.ALERT);
        status = sensorStatusRepo.getSensorStatus(uuid);
        assertEquals(Status.ALERT, status);
    }
}