package sample.monitor.service.measurement;

import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;
import sample.monitor.entity.Measurement;
import sample.monitor.repository.measurement.SensorMeasurementsRepo;
import sample.monitor.repository.measurement.SensorMeasurementsRepoInMemory;
import sample.monitor.service.status.SensorStatusService;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static org.junit.Assert.*;

public class SensorMeasurementsServiceImplTest {

    private static final UUID sensorUUID = UUID.randomUUID();
    private SensorMeasurementsService sensorMeasurementsService;
    private SensorMeasurementsRepo sensorMeasurementsRepo;
    private SensorStatusService sensorStatusService;
    private ArgumentCaptor<UUID> argumentCaptor;

    @Before
    public void setUp() throws Exception {
        sensorMeasurementsRepo = new SensorMeasurementsRepoInMemory();
        sensorStatusService = Mockito.mock(SensorStatusService.class);
        Mockito.doNothing().when(sensorStatusService).updateSensorStatus(sensorUUID);
        argumentCaptor = ArgumentCaptor.forClass(UUID.class);
        sensorMeasurementsService = new SensorMeasurementsServiceImpl(sensorMeasurementsRepo, sensorStatusService);
    }

    @Test
    public void addMeasurement() {

        Measurement measurementExpected = new Measurement(2000, LocalDateTime.now());
        sensorMeasurementsService.addMeasurement(sensorUUID, measurementExpected);
        Optional<Measurement> measurement = sensorMeasurementsRepo.getLatestMeasurement(sensorUUID);
        assertTrue(measurement.isPresent());
        assertEquals(measurementExpected, measurement.get());
        Mockito.verify(sensorStatusService).updateSensorStatus(argumentCaptor.capture());
        assertEquals(sensorUUID, argumentCaptor.getValue());
    }

    @Test
    public void getMeasurements() {

        Measurement measurementExpected1 = new Measurement(2000, LocalDateTime.now());
        Measurement measurementExpected2 = new Measurement(2000, LocalDateTime.now().plusMinutes(1));
        Measurement measurementExpected3 = new Measurement(2000, LocalDateTime.now().plusMinutes(2));
        sensorMeasurementsService.addMeasurement(sensorUUID, measurementExpected1);
        sensorMeasurementsService.addMeasurement(sensorUUID, measurementExpected3);
        sensorMeasurementsService.addMeasurement(sensorUUID, measurementExpected2);
        List<Measurement> measurementListExpected = Arrays.asList(measurementExpected3, measurementExpected2);
        List<Measurement> measurementList = sensorMeasurementsService.getMeasurements(sensorUUID, 2);
        assertEquals(measurementListExpected, measurementList);
    }
}