package sample.monitor.service.metric;

import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import sample.monitor.entity.Measurement;
import sample.monitor.repository.measurement.SensorMeasurementsRepo;
import sample.monitor.repository.measurement.SensorMeasurementsRepoInMemory;

import java.time.LocalDateTime;
import java.time.Period;
import java.util.Optional;
import java.util.OptionalDouble;
import java.util.UUID;

import static org.junit.Assert.*;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class SensorMetricsServiceImplTest {

    private static final UUID sensorUUID = UUID.randomUUID();
    private static int minutesIncrementer = 0;
    private static SensorMeasurementsRepo sensorMeasurementsRepo;
    private static SensorMetricsService sensorMetricsService;

    @BeforeClass
    public static void setUp() throws Exception {

        sensorMeasurementsRepo = new SensorMeasurementsRepoInMemory();
        sensorMetricsService = new SensorMetricsServiceImpl(sensorMeasurementsRepo);
        pushMeasurements(1000, 1500);
        pushMeasurements(1000, 2000);
        pushMeasurements(1000, 2500);
    }

    @Test
    public void _01_getSensorMaxMeasurement() {

        Optional<Measurement> measurement = sensorMetricsService.getSensorMaxMeasurement(sensorUUID, Period.ofDays(2));
        assertTrue(measurement.isPresent());
        assertEquals(2500, measurement.get().getCo2());
    }

    @Test
    public void _02_getSensorMeasurementAverage() {

        OptionalDouble average = sensorMetricsService.getSensorMeasurementAverage(sensorUUID, Period.ofDays(2));
        assertTrue(average.isPresent());
        assertEquals(2000d, average.getAsDouble(), 0);
    }

    private static void pushMeasurements(int count, int value) {
        while (count-- > 0) {
            Measurement measurement = new Measurement(value, LocalDateTime.now().plusMinutes(++minutesIncrementer));
            sensorMeasurementsRepo.addMeasurement(sensorUUID, measurement);
        }
    }
}