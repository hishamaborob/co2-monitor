package sample.monitor.service.status;

import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import sample.monitor.entity.Alert;
import sample.monitor.entity.Measurement;
import sample.monitor.entity.Status;
import sample.monitor.repository.alert.SensorAlertRepo;
import sample.monitor.repository.alert.SensorAlertRepoInMemory;
import sample.monitor.repository.measurement.SensorMeasurementsRepo;
import sample.monitor.repository.measurement.SensorMeasurementsRepoInMemory;
import sample.monitor.repository.status.SensorStatusRepo;
import sample.monitor.repository.status.SensorStatusRepoInMemory;

import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

import static org.junit.Assert.*;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class SensorStatusServiceImplTest {

    private static final UUID sensorUUID = UUID.randomUUID();
    private static int minutesIncrementer = 0;
    private static SensorStatusService sensorStatusService;
    private static SensorMeasurementsRepo sensorMeasurementsRepo;
    private static SensorStatusRepo sensorStatusRepo;
    private static SensorAlertRepo sensorAlertRepo;

    @BeforeClass
    public static void setUp() throws Exception {
        sensorMeasurementsRepo = new SensorMeasurementsRepoInMemory();
        sensorStatusRepo = new SensorStatusRepoInMemory();
        sensorAlertRepo = new SensorAlertRepoInMemory();
        sensorStatusService = new SensorStatusServiceImpl(sensorMeasurementsRepo, sensorStatusRepo, sensorAlertRepo);
    }


    @Test
    public void _01_updateSensorStatus() {

        pushMeasurements(3, 2001);
        sensorStatusService.updateSensorStatus(sensorUUID);
        Status status = sensorStatusRepo.getSensorStatus(sensorUUID);
        assertNotNull(status);
        assertEquals(Status.ALERT, status);
    }

    @Test
    public void _02_getSensorStatus() {

        pushMeasurements(2, 100);
        sensorStatusService.updateSensorStatus(sensorUUID);
        Status status = sensorStatusRepo.getSensorStatus(sensorUUID);
        assertEquals(Status.ALERT, status);

        Measurement measurement3 = new Measurement(1000, LocalDateTime.now().plusMinutes(++minutesIncrementer));
        sensorMeasurementsRepo.addMeasurement(sensorUUID, measurement3);

        sensorStatusService.updateSensorStatus(sensorUUID);
        status = sensorStatusRepo.getSensorStatus(sensorUUID);
        assertEquals(Status.OK, status);
    }

    @Test
    public void _03_getSensorAlerts() {

        List<Alert> alertList = sensorStatusService.getSensorAlerts(sensorUUID);
        assertNotNull(alertList);
        assertFalse(alertList.isEmpty());
        assertEquals(1, alertList.size());
    }

    @Test
    public void _04_getSensorStatusBackToWARN() {

        pushMeasurements(1, 2001);
        sensorStatusService.updateSensorStatus(sensorUUID);
        Status status = sensorStatusRepo.getSensorStatus(sensorUUID);
        assertEquals(Status.WARN, status);
    }

    @Test
    public void _05_getSensorStatusBackToOK() {

        pushMeasurements(1, 1000);
        sensorStatusService.updateSensorStatus(sensorUUID);
        Status status = sensorStatusRepo.getSensorStatus(sensorUUID);
        assertEquals(Status.OK, status);
    }

    @Test
    public void _06_getSensorStatusBackToALERT() {

        pushMeasurements(3, 2001);
        sensorStatusService.updateSensorStatus(sensorUUID);
        Status status = sensorStatusRepo.getSensorStatus(sensorUUID);
        assertNotNull(status);
        assertEquals(Status.ALERT, status);
    }

    private void pushMeasurements(int count, int value) {
        while (count-- > 0) {
            Measurement measurement = new Measurement(value, LocalDateTime.now().plusMinutes(++minutesIncrementer));
            sensorMeasurementsRepo.addMeasurement(sensorUUID, measurement);
        }
    }
}